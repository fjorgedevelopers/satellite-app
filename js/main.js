//jQuery UI Touch Punch
!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);

// JSON calculation for events. Minified from /js/calculation.json
var calc={option1:{hour1:"2",hour2:"2",hour3:"2",hour4:"2",hour5:"2",hour6:"3",hour7:"3",hour8:"3",hour9:"3",hour10:"3"},option2:{hour1:"2",hour2:"3",hour3:"3",hour4:"4",hour5:"4",hour6:"4",hour7:"4",hour8:"4",hour9:"4",hour10:"5"},option3:{hour1:"3",hour2:"4",hour3:"5",hour4:"6",hour5:"6",hour6:"7",hour7:"7",hour8:"7",hour9:"7",hour10:"7"},option4:{hour1:"5",hour2:"8",hour3:"10",hour4:"11",hour5:"12",hour6:"13",hour7:"13",hour8:"14",hour9:"14",hour10:"14"},option5:{hour1:"7",hour2:"12",hour3:"15",hour4:"16",hour5:"18",hour6:"18",hour7:"19",hour8:"20",hour9:"20",hour10:"21"},option6:{hour1:"10",hour2:"16",hour3:"19",hour4:"22",hour5:"24",hour6:"25",hour7:"25",hour8:"27",hour9:"27",hour10:"28"},option7:{hour1:"12",hour2:"20",hour3:"24",hour4:"27",hour5:"29",hour6:"31",hour7:"32",hour8:"33",hour9:"33",hour10:"34"},option8:{hour1:"14",hour2:"24",hour3:"28",hour4:"33",hour5:"35",hour6:"37",hour7:"37",hour8:"39",hour9:"41",hour10:"41"},option9:{hour1:"17",hour2:"27",hour3:"34",hour4:"38",hour5:"41",hour6:"42",hour7:"46",hour8:"46",hour9:"47",hour10:"48"},option10:{hour1:"20",hour2:"32",hour3:"38",hour4:"44",hour5:"48",hour6:"49",hour7:"50",hour8:"51",hour9:"53",hour10:"54"},option11:{hour1:"24",hour2:"39",hour3:"47",hour4:"54",hour5:"58",hour6:"62",hour7:"64",hour8:"66",hour9:"67",hour10:"68"},option12:{hour1:"30",hour2:"48",hour3:"61",hour4:"68",hour5:"73",hour6:"78",hour7:"80",hour8:"81",hour9:"82",hour10:"85"},option13:{hour1:"37",hour2:"57",hour3:"70",hour4:"81",hour5:"87",hour6:"92",hour7:"94",hour8:"99",hour9:"102",hour10:"104"},option14:{hour1:"42",hour2:"68",hour3:"84",hour4:"95",hour5:"100",hour6:"110",hour7:"111",hour8:"113",hour9:"116",hour10:"118"},option15:{hour1:"48",hour2:"77",hour3:"95",hour4:"107",hour5:"115",hour6:"120",hour7:"127",hour8:"131",hour9:"133",hour10:"136"},option16:{hour1:"60",hour2:"96",hour3:"112",hour4:"133",hour5:"146",hour6:"156",hour7:"159",hour8:"162",hour9:"165",hour10:"171"},option17:{hour1:"73",hour2:"114",hour3:"141",hour4:"163",hour5:"174",hour6:"184",hour7:"188",hour8:"194",hour9:"197",hour10:"201"},option18:{hour1:"95",hour2:"156",hour3:"188",hour4:"217",hour5:"231",hour6:"243",hour7:"249",hour8:"257",hour9:"266",hour10:"271"},option19:{hour1:"120",hour2:"192",hour3:"238",hour4:"267",hour5:"290",hour6:"305",hour7:"312",hour8:"322",hour9:"330",hour10:"337"},option20:{hour1:"177",hour2:"292",hour3:"357",hour4:"403",hour5:"432",hour6:"455",hour7:"470",hour8:"485",hour9:"491",hour10:"508"},option21:{hour1:"239",hour2:"378",hour3:"475",hour4:"515",hour5:"542",hour6:"562",hour7:"583",hour8:"593",hour9:"620",hour10:"633"},option22:{hour1:"2",hour2:"3",hour3:"3",hour4:"4",hour5:"4",hour6:"4",hour7:"4",hour8:"4",hour9:"4",hour10:"5"}};

$(document).ready(function() {

    // Replace all SVG images with inline SVG if img has .svg class

	jQuery('img.svg').each(function(){
	    var $img = jQuery(this);
	    var imgID = $img.attr('id');
	    var imgClass = $img.attr('class');
	    var imgURL = $img.attr('src');

	    jQuery.get(imgURL, function(data) {
	        // Get the SVG tag, ignore the rest
	        var $svg = jQuery(data).find('svg');

	        // Add replaced image's ID to the new SVG
	        if(typeof imgID !== 'undefined') {
	            $svg = $svg.attr('id', imgID);
	        }
	        // Add replaced image's classes to the new SVG
	        if(typeof imgClass !== 'undefined') {
	            $svg = $svg.attr('class', imgClass+' replaced-svg');
	        }

	        // Remove any invalid XML tags as per http://validator.w3.org
	        $svg = $svg.removeAttr('xmlns:a');

	        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
	        if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
	            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
	        }

	        // Replace image with new SVG
	        $img.replaceWith($svg);

	    }, 'xml');

	});

    // Mobile Menu Trigger

    $('#mobile-menu-trigger').on('click', function() {
        $(this).toggleClass('opened');
    });

    // Switch between Construction and Events Page
    $('.header-toggle a').on('click', function() {
        $('.header-toggle a').removeClass('active');
        $(this).addClass('active');

        if ($('#events-trigger').hasClass('active')) {
            $('#construction').hide();
            $('#event').show();
        } else {
            $('#construction').show();
            $('#event').hide();
        }
    });

    // Number Of Hours Slider
    $('#eventNumHours').slider({
        range: "max",
        min: 1,
        max: 10,
        value: 1,
        slide: function( event, ui ) {
            $('#amountHours').val(ui.value);
        }
    });
    $('#amountHours').val( $('#eventNumHours').slider('value') );

    // Percent of Women Slider
    $('#eventPercentWomen').slider({
        range: "max",
        min: 0,
        max: 100,
        value: 50,
        slide: function( event, ui ) {
            $('#amountWomen').val(ui.value);
        }
    });
    $('#amountWomen').val( $('#eventPercentWomen').slider('value') );

    // Remove Tooltip when select box is changed

    $('select').on('change', function() {
        $('.tooltip').removeClass('active');
    });

    // Calculate results when form is submitted on events or construction pages
    $('form').on('submit', function(e) {
        e.preventDefault();
        $('body').scrollTop(0);

        var page = $(this).closest('.page').attr('id');

        function updateResults() {

            // If page = event
            if (page === 'event') {
                var attendance = $('#eventAttendance').val();
                var hours = $('#eventNumHours').slider('value');
                var womenPercent = $('#eventPercentWomen').slider('value');

                if (womenPercent > 50) {
                    var womenDiff = womenPercent - 50;
                    var women = (womenDiff / 100) + 1;
                } else {
                    var women = 1;
                }

                // If attendance value is not changed, show error
                if (attendance == 0) {
                    $('#event .tooltip').addClass('active');
                } else {
                    $('.calculate-modal').show();

                    var attendanceKey = 'option' + attendance;
                    var hourKey = 'hour' + hours;

                    if ($("#alcohol").is(':checked')) {
                        var alcohol = 1.13; //13%
                    } else {
                        var alcohol = 1;
                    }

                    var result = Math.ceil(alcohol * calc[attendanceKey][hourKey] * women);
                }

            } else if (page === 'construction') {
                // If page = construction
                var numWorkers = $("#conNumWorkers").val();

                if (numWorkers == 0) {
                    $('#construction .tooltip').addClass('active');
                } else if (numWorkers >= 1 && numWorkers < 10){
                    $('.calculate-modal').show();
                    var result = numWorkers;
                } else if (numWorkers == 10) {
                    $('.calculate-modal').show();
                    var result = '1';
                    $('.con-10-units-text').show();
                    $('.units-text').hide();
                }

            }

            $('#result').html(result);
        }

        updateResults();
    });

    $('.reset').on('click', function() {
        $('.calculate-modal').hide();

        $('#eventAttendance').val(0);
        $('#eventNumHours').slider('value', 1);
        $('#amountHours').val(1);
        $('#eventPercentWomen').slider('value', 50);
        $('#amountWomen').val(50);
        $("#conNumWorkers").val(0);
        $('.con-10-units-text').hide();
        $('.units-text').show();
    });

}); // end document ready
